---
name: Color
---

This page includes [color swatches](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-product-marketing/brand-design/-/tree/e6e2bb24e899078935d1aeb0e65c226b6bf36a8b/brand/brand-assets/brand-color-palettes) for our brand colors. The colors are denoted with a *p* for primary, and *s* for secondary. Default to the following applications when using primary and secondary colors:

- **Backgrounds** can use gradients or select solid colors: white, Charcoal, or purple
- **Graphical elements** should default to colors in the primary palette; the colors in the secondary palette can be used as additional accents.
- **Headlines and body copy** should use white or charcoal.
- **Calls to action (CTAs) and buttons** should use white or charcoal.
- **Hyperlinks** commonly use Purple 01p on dark backgrounds or Purple 02p on light backgrounds.
- **Small titles and data points** can have color applied sparingly and with discretion for accessibility with a contrast minimum of 1.4.3 (Level AA) according to [WCAG 2.1](https://www.w3.org/WAI/WCAG21/Understanding/contrast-minimum).
- `NOTE:` Colors other than white or Charcoal should not be applied in sentences or large bodies of copy.

## Primary colors

The primary palette features colors from our core logo and our signature purple. These colors are most frequently used and associated with our brand.

### Primary color palette

| **Swatch**                                                                                                          | **HEX** | **RGB**       | **CMYK**       | **PMS**                        |
| ------------------------------------------------------------------------------------------------------------------- | ------- | ------------- | -------------- | ------------------------------ |
| <div class="gl-p-3" style="background-color:#ffffff;"><span class="variable">White</span> </div>                    | #FFFFFF | 255, 255, 255 | 0, 0, 0, 0     | White                          |
| <div class="gl-p-3 gl-text-white" style="background-color:#171321;"><span class="variable">Charcoal</span> </div>   | #171321 | 23, 19, 33    | 90, 68, 41, 90 | 433 C / 4280 U                 |
| <div class="gl-p-3" style="background-color:#FCA326;"><span class="variable">Orange 01p</span> </div>               | #FCA326 | 252, 161, 33  | 0, 40, 80, 5   | 143 C / 1365 U                 |
| <div class="gl-p-3" style="background-color:#FC6D26;"><span class="variable">Orange 02p</span> </div>               | #FC6D26 | 252, 109, 38  | 0, 60, 80, 5   | 716 C / 144 U                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#E24329;"><span class="variable">Orange 03p</span> </div> | #E24329 | 226, 67, 41   | 0, 75, 80, 10  | 7417 C / 1665 U                |
| <div class="gl-p-3" style="background-color:#A989F5;"><span class="variable">Purple 01p</span> </div>               | #A989F5 | 169, 137, 245 | 53, 55, 0, 0   | 2655 C / 2645 CU               |
| <div class="gl-p-3 gl-text-white" style="background-color:#7759C2;"><span class="variable">Purple 02p</span> </div> | #7759C2 | 119, 89, 194  | 64, 70, 0, 0   | 2665 C / 2593 U                |

## Secondary colors

The secondary palette expands upon our oranges and purples to provide more color varieties. These additional hues can be used to highlight various elements and add visual depth where needed. Adhere to the guidelines below regarding the colors in this palette:

- The teals carry a positive connotation, especially when used to reference product features.
- The pink is reserved exclusively for gradients.
- The cool grays offer neutral color options to be used for differentiating information and/or establishing hierarchy.

### Secondary color palette

| **Swatch**                                                                                                          | **HEX** | **RGB**       | **CMYK**       | **PMS**                        |
| ------------------------------------------------------------------------------------------------------------------- | ------- | ------------- | -------------- | ------------------------------ |
| <div class="gl-p-3" style="background-color:#CEB3EF;"><span class="variable">Purple 01s</span> </div>                  | #CEB3EF | 206, 179, 239 | 24, 29, 0, 0    | 2635 C |
| <div class="gl-p-3 gl-text-white" style="background-color:#5943B6;"><span class="variable">Purple 02s</span> </div>                  | #5943B6 | 89, 67, 182 | 77, 82, 0, 0 | 2097 C                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#2F2A6B;"><span class="variable">Purple 03s</span> </div>    | #2F2A6B | 47, 42, 107 | 98, 98, 27, 15 | 273 C                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#232150;"><span class="variable">Purple 04s</span> </div>    | #232150 | 35, 33, 80    | 85, 82, 42, 38 | 275 C                 |
| <div class="gl-p-3" style="background-color:#FDF1DD;"><span class="variable">Orange 01s</span> </div>    | #FDF1DD | 253, 241, 221    | 1, 4, 12, 0 | 7506 C             |
| <div class="gl-p-3" style="background-color:#FFB9C9;"><span class="variable">Pink 01s</span> </div>    | #FFB9C9 | 255, 185, 201    | 0, 33, 10, 0 | 1767 C / 1767 U             |
| <div class="gl-p-3" style="background-color:#C5F4EC;"><span class="variable">Teal 01s</span> </div>    | #C5F4EC | 197, 244, 236    | 20, 0, 11, 0 | 317 C             |
| <div class="gl-p-3" style="background-color:#6FDAC9;"><span class="variable">Teal 02s</span> </div>                  | #6FDAC9 | 111, 218, 201 | 51, 0, 29, 0    | 3242 C |
| <div class="gl-p-3" style="background-color:#10B1B1;"><span class="variable">Teal 03s</span> </div>                  | #10B1B1 | 16, 177, 177 | 75, 5, 35, 0 | 7710 C                  |
| <div class="gl-p-3" style="background-color:#D1D0D3;"><span class="variable">Gray 01</span> </div>                  | #D1D0D3 | 209, 208, 211 | 8, 5, 7, 16    | Cool Gray 3 C /  Cool Gray 2 U |
| <div class="gl-p-3" style="background-color:#A2A1A6;"><span class="variable">Gray 02</span> </div>                  | #A2A1A6 | 162, 161, 166 | 19, 12, 13, 34 | 422 C / 422 U                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#74717A;"><span class="variable">Gray 03</span> </div>    | #74717A | 116, 113, 122 | 30, 20, 19, 58 | 424 C / 425 U                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#45424D;"><span class="variable">Gray 04</span> </div>    | #45424D | 69, 66, 77    | 41, 28, 22, 70 | 7540 C / 419 U                 |
| <div class="gl-p-3 gl-text-white" style="background-color:#2B2838;"><span class="variable">Gray 05</span> </div>    | #2B2838 | 43, 40, 56    | 94, 77, 53, 94 | 426 C / Black 6 U              |

## Gradients

Gradients are an essential part of our brand and a key way that we incorporate color into marketing collateral. They help create a sense of movement and motion while also providing depth to an asset. Linear gradients are often used on graphics and linework, while freeform gradients are used to add visual interest to the background.

### Linear gradients

Our brand colors can be categorized into three color families: *purple*, *orange*, and *teal*. One-step or two-step gradients can be created within their respective color family, meaning that the hues within a single gradient are only one or two shades away from one another. Follow the steps below for creating these types of gradients:

- Start by selecting a color family. Then choose a color within the family.
- From that color you can move either one or two tones away to select the second color for the gradient.
- While the direction of the gradient can vary, the gradient should be set to linear.
- `NOTE:` our AI gradient is an exception from the usual one-step and two-step gradients. This linear gradient goes from Orange 01p to Purple 01p and should only be used for content focused on AI and/or GitLab Duo.

<figure-img alt="Examples of approved linear gradients" label="One-step and two-step gradient examples" src="/img/brand/linear-gradients.svg"></figure-img>

### Freeform gradients

Freeform gradients are generally reserved for backgrounds. The default freeform gradient for our core brand consists of Orange 02p, Purple 01p, and Pink 01s with a white base fill. In some cases, these colors can be customized to create a more curated aesthetic to differentiate an asset from general-branded designs. Follow the parameters below to ensure visual consistency across all freeform gradients:

- The gradient should feel organic and not distract from other elements on the design.
- The gradient should use the freeform gradient feature in Illustrator, or by creating layered radial gradients in Figma.
- They should be limited to no more than 3 colors, not including the base fill.
- Colors should feel soft and gently blur into one another; in most cases, keeping opacities below 30% achieves this look.
- Purple should always be included as one of the gradient’s colors.
- Charcoal and gray should not be used.
- Light-mode gradients should use white as the base fill.
- Dark-mode gradients should use Purple 04s as the base fill.
- When adding text or our logo, ensure that they are placed on a part of the gradient that provides adequate contrast.

<figure-img alt="Freeform gradient sample" label="Example of the default freeform gradient" src="/img/brand/freeform-gradient.png"></figure-img>

## Contrast and accessibility

Using color combinations that have sufficient contrast benefits everyone and preserves readability. A contrast ratio of 4.5:1 or higher is preferred for text and UI elements, but 3:1 can be used for larger text. Illustrations are an exception and will use a larger range of the palette, although even the key elements should still be legible. Color accessibility enables people with visual impairments or color vision deficiencies to interact with digital experiences in the same way as their non-visually-impaired counterparts. For more information about color contrast in a digital context, view the [WCAG Success Criterion 1.4.3 Contrast (Minimum)](https://www.w3.org/TR/WCAG21/#contrast-minimum) guidelines.

Below are the approved font color pairings for optimal contrast and accessibility. Always pair copy with a high-contrast background; this applies to gradient backgrounds as well.

<figure-img alt="Contrast examples for typography" label="Accessible font color pairings" src="/img/brand/colors-accessibility.svg"></figure-img>

## Terminology

### Digital

**Digital colors** refer to colors that will be produced digitally. For instance, the colors on this webpage use HEX codes to generate the colors. If you are working with any digital asset, you can input either the HEX or RGB color codes, as detailed in the tables above. More digital use cases include: digital ads, digital billboards, video and animation, slide decks, and Canva.

- **HEX:** HEX codes are based on the hexadecimal system in computing. HEX and RGB codes provide the same information, but in different formats. The HEX code includes a hashtag followed by six characters.
  - **Example:** `#FC6D26`
- **RGB:** RGB is an acronym for Red, Green, and Blue. All digital colors are generated using percentages of these three colors. As mentioned, RGB is interchangeable with HEX.
  - **Example:** `rgb(255, 255, 255)`

### Print

**Print colors** refer to colors that will be generated in printed assets. Generally speaking, digital screens saturate colors more deeply than printed materials do, so print-specific colors codes are used to create more visual consistency between the two formats. Some print use cases include: pop up banners, swag and merchandise, stickers, print billboards, and booth designs.

- **CMYK:** CMYK is an acronym for Cyan, Magenta, Yellow, and Key. If an asset is a 'full-color' print, then CMYK is used. CMYK is usually the default for general printing and can be found in personal and commercial printers alike.
  - **Example:** C:90, M:68, Y:41, K:90
- **PMS:** PMS is an acronym for Pantone Matching System. Whereas CMYK colors are generated by ink values, PMS colors are pre-mixed, universal colors established by [Pantone](https://www.pantone.com/). This technique provides the most consistent color across materials. PMS is used for **spot color** prints (examples include embroidery and screen printing) and for more advanced print jobs to ensure quality and accuracy; vendors will usually specify when PMS is required. PMS colors have either a name or a code, which consists of a set of numbers followed by either a 'C' for **coated** or a 'U' for **uncoated**. Coated and uncoated refer to the paper; coated indicates gloss and uncoated refers to no coating/gloss (ie: matte paper).
  - **Example:** 433 C / 4280 U
